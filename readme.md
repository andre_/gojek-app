# Introduction

Here I provided driver's notification app build with:

* React JS
* Webpack
* SASS as CSS Preprocessor
* Jest & Enzyme (for testing purpose)

To reduce npm package, I use React Context instead of Redux and its
dependencies.

I use json generator for generate more data to the table. [link](https://www.json-generator.com/#)

---

# Installation

For development version:

* Install all npm packages and dependencies using command `yarn` or `yarn install`
* Run `yarn dev` on terminal
* Open browser and go to `https://localhost:3000`

For production version:

* Make sure all packages and dependencies have installed (just run `yarn` to
  install dependencies)
* Run `yarn build`
* All compiled files will be in `dist` folder

For testing:

* Make sure all packages and dependencies have installed (just run `yarn` to
  install dependencies)
* Run `yarn test`

---

# Project Structure

```
__tests__
  components
dist
App.js
setupTest.js
src
  components
  context
  modules
  services
  styles
webpack
```

* \*tests
  * This is the folder for unit testing. Jest / Enzyme will execute testing from this folder.
* \*components
  * This is where testing components live.
* \*dist
  * If you run app with `yarn build` (build version), all of compiled files will be here.
* \*App.js
  * Base component, contain table, buttons and anything else.
* \*setupTest.js
  * A files for jest environtment setup
* \*src
  * Place for any components, services, context, styles, etc.
  * components
    * directory for reusable components
  * context
    * I dont use redux in this apps. So I set all of React context thing in this
      folder.
  * services
    * I create a function to make paginator base on array of object, also I save
      the json files in this folder
  * styles
    * Like the name. I save all of SCSS configurations like breakpoint, colors
      in this place.
* \*webpack
  * I make a separate files for webpack development version and webpack
    production version. Also I include a base configs for webpack.

# Extra Effort

* I took an extra effort when designing Responsive Design because it build with no-css library such a bootstrap, bulma, etc.
  But still no problem because css3 already has grid, flex, media query and etc.
* Actually still easy to build a modal component. The hard part is when I want
  to render the modal just when the user click the notify button. Because before
  this, modal renderred even I'm not click notify button. So I make conditional
  to make sure modal renderred just when I click notify button.

## Go to [this link](https://gojek-notify.netlify.com) to access in a live-production version.
