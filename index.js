import React from 'react';
import {render} from 'react-dom';
import App from './src/App';

import {Provider, Consumer} from './src/context/Provider';

const AppContainer = () => (
  <Provider>
    <App />
  </Provider>
);

render(<AppContainer />, document.getElementById('app-container'));
