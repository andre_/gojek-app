import React from 'react';
import {render, cleanup} from 'react-testing-library';
import Table from '../../src/components/table';

describe('Table', () => {
  let props;
  beforeEach(() => {
    props = {
      data: [],
    };
  });

  test('Render table correctly', () => {
    const renderTable = render(<Table {...props} />);

    expect(renderTable).toBeTruthy();
  });

  afterEach(cleanup);
});
