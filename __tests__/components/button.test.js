import React from 'react';
import {render, cleanup} from 'react-testing-library';
import {shallow} from 'enzyme';
import Button from '../../src/components/button';

afterEach(cleanup);

describe('Button component', () => {
  test('Button render correctly', () => {
    const renderButton = render(<Button title="submit" />);

    expect(renderButton).toBeTruthy();
  });

  test('Click event', () => {
    const mockFn = jest.fn();
    const wrap = shallow(<Button title="submit" onClick={mockFn} />);

    wrap.find('button').simulate('click');
    expect(mockFn.mock.calls.length).toEqual(1);
  });
});
