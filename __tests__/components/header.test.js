import React from 'react';
import {render, cleanup} from 'react-testing-library';
import {shallow, mount} from 'enzyme';
import Header from '../../src/components/header';

afterEach(cleanup);

describe('Header Component', () => {
  test('Render header correctly', () => {
    const renderHeader = shallow(<Header />);

    expect(renderHeader).toBeTruthy();
  });

  test('Render header with function', () => {
    const mockFn = jest.fn();
    const wrap = mount(<Header onSearch={mockFn} />);

    wrap.find('input').simulate('keyUp', {keyCode: 40});
    expect(mockFn.mock.calls).toBeTruthy();
  });
});
