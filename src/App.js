import React from 'react';
import './styles/main.scss';
import {Context, Consumer} from './context/Provider';

import Button from './components/button';
import Table from './components/table/';
import Header from './components/header';

import {drivers} from './services/drivers.json';
import Paginator from './services/paginator';

class App extends React.Component {
  state = {
    loading: false,
    page: 1,
  };

  componentDidMount() {
    this.initDriver();
  }

  initDriver = () => {
    const {addDriver} = this.context;
    const {data} = Paginator(drivers, this.state.page, 5);

    this.context.addDriver(data);
  };

  getMoreDriver = () => {
    return new Promise((resolve, reject) => {
      try {
        this.setState({page: (this.state.page += 1)}, () => {
          setTimeout(() => {
            const {data} = Paginator(drivers, this.state.page, 2);
            resolve(data);
          }, 1000);
        });
      } catch (e) {
        reject(e);
      }
    });
  };

  loadMore = () => {
    const {addDriver} = this.context;
    this.setState({loading: true});

    this.getMoreDriver()
      .then(result => {
        addDriver(result);
        this.setState({
          loading: false,
        });
      })
      .catch(err => {
        this.setState({loading: false});
      });
  };

  render() {
    return (
      <Consumer>
        {data => (
          <>
            <Header onSearch={data.searchDriver} />

            <section className="table-wrapper">
              <Table data={data.driverTemp} />
            </section>

            <section className="button-wrapper">
              <Button
                title="LOAD MORE"
                type="info"
                loading={this.state.loading}
                size="large"
                onClick={this.loadMore}
              />
            </section>
          </>
        )}
      </Consumer>
    );
  }
}

App.contextType = Context;

export default App;
