import PropTypes from 'prop-types';

const Paginator = (obj, page = 1, perPage = 10) => {
  let offset = (page - 1) * perPage;
  let paginatedItems = obj.slice(offset).slice(0, perPage);
  let totalPages = Math.ceil(obj.length / perPage);

  return {
    page,
    perPage,
    nextPage: totalPages > page ? page + 1 : null,
    total: obj.length,
    totalPages,
    data: paginatedItems,
  };
};

Paginator.propTypes = {
  obj: PropTypes.array.isRequired,
  page: PropTypes.number.isRequired,
  perPage: PropTypes.number,
};

export default Paginator;
