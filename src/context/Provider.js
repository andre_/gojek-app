import React from 'react';

const Context = React.createContext();
const Consumer = Context.Consumer;

class Provider extends React.Component {
  state = {
    drivers: [],
    driverTemp: [],
  };

  addDriver = driver => {
    this.setState({
      drivers: [...this.state.drivers, ...driver],
      driverTemp: [...this.state.drivers, ...driver],
    });
  };

  searchDriver = text => {
    let copyDriver = this.state.drivers;

    copyDriver = copyDriver.filter(v => {
      return v.name.toLowerCase().search(text.toLowerCase()) !== -1;
    });

    this.setState({
      driverTemp: copyDriver,
    });
  };

  render() {
    return (
      <Context.Provider
        value={{
          drivers: this.state.drivers,
          driverTemp: this.state.driverTemp,
          addDriver: this.addDriver,
          searchDriver: this.searchDriver,
        }}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export {Provider, Context, Consumer};
