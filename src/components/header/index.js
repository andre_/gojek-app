import React from 'react';

function Header(props) {
  function searchDriver(text) {
    props.onSearch(text);
  }

  return (
    <div className="header">
      <div className="logo">
        <h4>Go-Notif!</h4>
      </div>
      <input
        placeholder="Search driver"
        onKeyUp={e => searchDriver(e.target.value)}
      />
    </div>
  );
}

export default Header;
