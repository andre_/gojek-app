import React from 'react';
import PropTypes from 'prop-types';

const Button = props => (
  <button
    className={`btn btn-${props.type} btn-${props.size} ${
      props.loading ? 'btn-is-loading' : null
    }`}
    onClick={props.onClick}>
    {props.loading ? 'Loading...' : props.title}
  </button>
);

Button.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.string,
  size: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  type: 'default',
  size: 'normal',
};

export default Button;
