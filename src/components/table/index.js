import React, {Component} from 'react';
import Modal from '../modal';
import Button from '../button';

import Paginator from '../../services/paginator';

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      page: 1,
    };
  }

  toggleModal = () => {
    this.setState({showModal: !this.state.showModal});
  };

  render() {
    const {data} = this.props;

    return (
      <>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Suspended</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {data.length ? (
              data.map((v, k) => (
                <tr key={k} className={v.suspended ? 'suspend-row' : null}>
                  <td>{v.name}</td>
                  <td>{v.phone}</td>
                  <td>{v.email}</td>
                  <td>{v.suspended ? 'YES' : 'NO'}</td>
                  <td>
                    {!v.suspended ? (
                      <Button
                        title="NOTIFY"
                        type="danger"
                        onClick={this.toggleModal}
                      />
                    ) : (
                      '-'
                    )}
                  </td>
                </tr>
              ))
            ) : (
              <tr>
                <td colSpan="5">Driver not found</td>
              </tr>
            )}
          </tbody>
        </table>

        {this.state.showModal ? <Modal onClose={this.toggleModal} /> : null}
      </>
    );
  }
}

export default Table;
