import React from 'react';
import Button from '../button';

class Modal extends React.Component {
  state = {
    message: '',
  };

  componentDidMount() {
    document.addEventListener('keydown', this.listenEsc, false);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.listenEsc, false);
  }

  listenEsc = () => {
    if (event.keyCode === 27) {
      this.props.onClose();
    }
  };

  dismissModal = () => {
    this.setState(
      {
        message: '',
      },
      () => this.props.onClose(),
    );
  };

  render() {
    return (
      <div className="modal modal-show">
        <button className="modal-close" onClick={this.dismissModal}>
          X
        </button>
        <div className="modal-body">
          <div className="form-group">
            <textarea
              className="textarea"
              rows="10"
              onKeyUp={e => this.setState({message: e.target.value})}
            />
          </div>

          <div className="bottom-group">
            <Button title="Cancel" onClick={this.dismissModal} />
            <Button title="Send" type="success" onClick={this.dismissModal} />
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
